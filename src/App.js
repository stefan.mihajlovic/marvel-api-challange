import React from 'react';
import SignUp from './components/auth/SignUp/SignUp';
import {Route, BrowserRouter} from 'react-router-dom'
import Home from './components/Home/Home';
import Login from './components/auth/Login/Login';
import Navbar from './components/Navbar/Navbar'
import Bookmarks from './components/Bookmarks/Bookmarks';
import CharacterPage from './components/CharacterPage/CharacterPage';
import UserProfile from './components/auth/UserProfile/UserProfile';
import Redirect from './components/Redirect/Redirect';
import './app.scss'

function App() {
  return (
    <BrowserRouter>
      <div className="app-container">
        <Navbar />
        <Route exact path='/' component={Home} />
        <Route path='/signup' component={SignUp} />
        <Route path='/login' component={Login} />
        <Route path='/bookmarks' component={Bookmarks} />
        <Route path='/characters/:id' component={CharacterPage} />
        <Route path='/user_profile' component={UserProfile} />
        <Route path='/redirect/:link' component={Redirect} />
      </div>
    </BrowserRouter>
  );
}

export default App;
