import React from 'react'
import { Link } from 'react-router-dom'
import './bookmark-card.scss'

const BookmarkCard = (props) => {
    const {bookmark} = props

    return (
        <div id={`bookmark-${bookmark.id}`} className='bookmark-card-container js-bookmark-card'>
        <img src={`${bookmark.thumbnail.path}/portrait_xlarge.${bookmark.thumbnail.extension}`} alt="char thumb" className='bookmark-image'/>
        <div className='bookmark-title-description-container'>
            <Link to={`/characters/${bookmark.id}`} className="bookmark-title"> {bookmark.name} </Link>
            <p className="bookmark-description"> {bookmark.description !== '' ? bookmark.description : `(No description)`} </p>
        </div>
            <img src={require('../../../images/Charater-cards/cpt-amer-color.png')} alt="cpt amer color" className='remove-bookmark-icon' onClick={() => {props.removeBookmark(bookmark.id)}}/>
    </div>
    )
}

export default BookmarkCard
