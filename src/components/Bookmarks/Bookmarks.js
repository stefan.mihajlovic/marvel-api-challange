import React, {Component} from 'react'
import BookmarkCard from './BookmarkCard/BookmarkCard'
import './bookmarks.scss'

class Bookmarks extends Component {
    state = {
        bookmarks:[],
        loaded:false,
        currrentPage:1,
        charactersPerPage: 12
    }

    componentDidMount() {
        if (localStorage.getItem('loggedUser')) {
            const userId = Number(localStorage.getItem('loggedUser'))
            const allUsers = JSON.parse(localStorage.getItem('users'))
            const loggedUsersIndex = allUsers.map(user => {return user.id}).indexOf(userId)
            const loggedUser = allUsers[loggedUsersIndex]
            this.setState({bookmarks:loggedUser.bookmarks,loaded:true})
        }  else {
            this.props.history.push('/login')
        }    
    }
    changePage = (event) => {
        this.setState({currrentPage:event.target.id})
    }

    removeBookmark = (bookmarkId) => {
        const userId = Number(localStorage.getItem('loggedUser'))
        const allUsers = JSON.parse(localStorage.getItem('users'))
        const loggedUsersIndex = allUsers.map(user => {return user.id}).indexOf(userId)
        const loggedUser = allUsers[loggedUsersIndex]
        const charIndex = loggedUser.bookmarks.map(bookmark => {return bookmark.id}).indexOf(bookmarkId)
        loggedUser.bookmarks.splice(charIndex, 1)
        allUsers[loggedUsersIndex] = loggedUser
        localStorage.setItem('users', JSON.stringify(allUsers))
        this.setState({bookmarks:loggedUser.bookmarks})
    }

    render() {
        const {bookmarks, loaded, currrentPage, charactersPerPage} = this.state
        const indexOfLastCharacter = currrentPage * charactersPerPage
        const indexOfFirstCharacter = indexOfLastCharacter - charactersPerPage
        const displayedBookmarks = bookmarks.slice(indexOfFirstCharacter, indexOfLastCharacter)
        const renderBookmarks = displayedBookmarks.map(bookmark => {
            return (
                <BookmarkCard key={bookmark.id} bookmark={bookmark} removeBookmark={this.removeBookmark}/>
            )
        })


        const pageNumbers = []
        for (let i=1; i<= Math.ceil(bookmarks.length/charactersPerPage); i++) {
            pageNumbers.push(i)
        }
        const renderPageNums = pageNumbers.map(number => {
            return (
                <React.Fragment key={number}>
                    <input type="radio" name="pagination radio" className='bookmark-pagination-radio'
                     id={`bookmark-radio-${number}`} 
                     defaultChecked={number === currrentPage ? true : false}/>
                    <label onClick={this.changePage} htmlFor={`bookmark-radio-${number}`} id={number} className='bookmark-pagination-number'> {number} </label>
                </React.Fragment>
            )
        })
        return (
            <div className='bookmarks-container js-bookmarks-container'>
                {loaded ?
                    bookmarks.length > 0 ? <React.Fragment>
                                                <div className="bookmark-cards-container">
                                                    {renderBookmarks} 
                                                </div>
                                                <div className='bookmark-pagination'>{renderPageNums}</div>
                                            </React.Fragment>  
                        : 
                    <div className='no-bookmarks'>NO BOOKMARKS!!!</div>
                :
                    <div>Loading</div>}
            </div>
        )
    }
}

export default Bookmarks
