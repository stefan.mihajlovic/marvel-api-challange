import React from 'react'
import {Link} from 'react-router-dom'
import './character-info.scss'

const CharacterInfo = (props) => {
    const { character, comics, events } = props
    const users = JSON.parse(localStorage.getItem('users'))
    const user = users[Number(localStorage.getItem('loggedUser'))]
    const alreadyBookmarked = user.bookmarks.map(bookmark => {return bookmark.id}).indexOf(character.id)
    const addRemoveBookmark = (event) => {
        const userId = Number(localStorage.getItem('loggedUser'))
        const allUsers = JSON.parse(localStorage.getItem('users'))
        const loggedUsersIndex = allUsers.map(user => {return user.id}).indexOf(userId)
        const updatedUser = allUsers[loggedUsersIndex]
        const bookmarkIndex = updatedUser.bookmarks.map(bookmark => {return bookmark.id}).indexOf(character.id)    
        event.target.classList.toggle('char-info-bookmarked')
        if ( bookmarkIndex < 0) {
            updatedUser.bookmarks.push(character)
        } else {

            updatedUser.bookmarks.splice(bookmarkIndex, 1)
        }
        allUsers[loggedUsersIndex] = updatedUser
        localStorage.setItem('users', JSON.stringify(allUsers))
    }    
    
    let j, x, i;
    for (i = comics.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = comics[i];
        comics[i] = comics[j];
        comics[j] = x;
    }
    let m,n,s
    for (s = events.length - 1; s > 0; s--) {
        m = Math.floor(Math.random() * (s + 1));
        n = events[s];
        events[s] = events[m];
        events[m] = n;
    }
    const renderEvents = events.slice(0,3).map(event => {
        return (
            <div key={event.id} className="event-container">
                <Link to={`/redirect/${event.urls[0].url}`} target='_blank' className="event-title"> {event.title} </Link>
                <p className="event-description"> {event.description} </p>
            </div>
        )
    })
    const renderComics = comics.slice(0, 3).map(comic => {
        return (
            <div key={comic.id} className='comic-container'>
                <div className='comic-inner'>
                    <div className='comic-front'>
                        <img src={`${comic.thumbnail.path}/portrait_uncanny.${comic.thumbnail.extension}`} className='comic-image' alt='comic-cover' />
                    </div>

                    <div className='comic-back'>
                        <h3 className='comic-title'> {comic.title} </h3>
                        <div className='comic-info'> {`Issue number: #${comic.issueNumber}`} </div>
                        <div className='comic-info'> {`Number of Pages: ${comic.pageCount}`} </div>
                        <div className='comic-info'> {`${comic.prices[0].type}: ${comic.prices[0].price}$`} </div>
                    </div>
                </div>
            </div>)
    })
    return (
        <div className='character-info-container'>
                <img src={require('../../../images/Charater-cards/cpt-amer-no-color.png')} alt="cpt amer no color" 
                className='character-info-bookmarks-icon-back'/>
                
                <img src={require('../../../images/Charater-cards/cpt-amer-color.png')} alt="cpt amer no color" onClick={addRemoveBookmark}
                className={`character-info-bookmarks-icon-front ${alreadyBookmarked > -1 ? `char-info-bookmarked` : ``}`} />

            <div className='character-info-text-image'>
                <img src={`${character.thumbnail.path}/portrait_uncanny.${character.thumbnail.extension}`} alt="char thumb" className='character-info-image' />
                <div className='character-info-text'>
                    <h1 className='character-info-name'>
                        {character.name}
                    </h1>
                    <p className='character-info-desc'>
                        {character.description !== '' ? character.description : `(No character description)`}
                    </p>
                </div>
            </div>
                {events.length === 0 ? 
                <div className='no-events'>No events</div> : <div className='all-events-container'>{renderEvents}</div>}
           { comics.length !== 0 && <h2 className='all-comics-title'>Comics:</h2> }
            <div className='all-comics-container'>
                {comics.length === 0 ? <div className='no-comics'>No comics</div> : renderComics}
            </div>
        </div>

    )
}

export default CharacterInfo
