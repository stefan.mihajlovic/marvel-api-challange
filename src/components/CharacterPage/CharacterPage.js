import React, { Component } from 'react'
import axios from  'axios'
import CharacterInfo from './CharacterInfo/CharacterInfo'
import './character-page.scss'

class CharacterPage extends Component {
    state = {
        character: '',
        comics: [],
        events: [],
        loaded: false
    }

    componentDidMount() {
        const id = this.props.match.params.id
        const url = `https://gateway.marvel.com:443/v1/public/characters/${id}?ts=1&apikey=031ff7298eee579ead8f58d2288e0fc3&hash=ea44baf09b4da5db0380665be26ba9d3`
        const url2 = `https://gateway.marvel.com:443/v1/public/characters/${id}/comics?ts=1&apikey=031ff7298eee579ead8f58d2288e0fc3&hash=ea44baf09b4da5db0380665be26ba9d3`        
        const url3 = `https://gateway.marvel.com:443/v1/public/characters/${id}/events?ts=1&apikey=031ff7298eee579ead8f58d2288e0fc3&hash=ea44baf09b4da5db0380665be26ba9d3`

        const axiosRequest1 = axios.get(url)
        const axiosRequest2 = axios.get (url2)
        const axiosRequest3 = axios.get(url3)
        axios.all([axiosRequest1,axiosRequest2, axiosRequest3]).then(axios.spread((...responses) => {
            this.setState({character:responses[0],comics:responses[1],events:responses[2],loaded:true})
        }))
    }
    render() {
        if(!localStorage.getItem('loggedUser')) {
            this.props.history.push('/login')
        } 
        const {character,comics,events,loaded} = this.state
        return (
            <div className='character-page-container'>
                {
                    loaded === true ?
                    <CharacterInfo character = {character.data.data.results[0]} comics={comics.data.data.results} events={events.data.data.results} />
                    :
                    <div className='loading-character'>Loading....</div>
                }
             
            </div>
        )
    }
}

export default CharacterPage
