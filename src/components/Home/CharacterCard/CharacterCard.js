import React from 'react'
import './character-card.scss'
import { Link } from 'react-router-dom'

const CharacterCard = (props) => {
    const {character} = props
    const users = JSON.parse(localStorage.getItem('users'))
    const user = users[Number(localStorage.getItem('loggedUser'))]
    const alreadyBookmarked = user.bookmarks.map(bookmark => {return bookmark.id}).indexOf(character.id)
    const addToBookmarks = (event) => {
        const userId = Number(localStorage.getItem('loggedUser'))
        const allUsers = JSON.parse(localStorage.getItem('users'))
        const loggedUsersIndex = allUsers.map(user => {return user.id}).indexOf(userId)
        const updatedUser = allUsers[loggedUsersIndex]
        const bookmarkIndex = updatedUser.bookmarks.map(bookmark => {return bookmark.id}).indexOf(character.id)    
        event.target.classList.toggle('bookmarked')
        if ( bookmarkIndex < 0) {
            updatedUser.bookmarks.push(character)
        } else {

            updatedUser.bookmarks.splice(bookmarkIndex, 1)
        }
        allUsers[loggedUsersIndex] = updatedUser
        localStorage.setItem('users', JSON.stringify(allUsers))
    }
    return (
        <div className='character-card-container'>
            <img src={`${character.thumbnail.path}/portrait_xlarge.${character.thumbnail.extension}`} alt="char thumb" className='character-image'/>
            <div className="character-title-description-container">
              <Link to={`/characters/${character.id}`} className="card-title">{character.name}</Link>
                <p className="character-description"> {character.description !== '' ? character.description : `(no description)`} </p>
            </div>
                <img src={require('../../../images/Charater-cards/cpt-amer-no-color.png')} alt="cpt amer no color" 
                className='add-to-bookmarks-icon-back'/>
                
                <img src={require('../../../images/Charater-cards/cpt-amer-color.png')} alt="cpt amer no color" 
                className={`add-to-bookmarks-icon-front ${alreadyBookmarked > -1 ? `bookmarked` : ``}`} onClick={addToBookmarks}/>
        </div>
    )
}

export default CharacterCard
