import React, { Component } from 'react'
import CharacterCard from './CharacterCard/CharacterCard'
import axios from 'axios'
import './home.scss'

class Home extends Component {

    state = {
        characterName: '',
        results: [],
        currrentPage: 1,
        charactersPerPage: 12,
        searchFinished: false
    }

    getCharacters = (event) => {
        this.setState({
            characterName: event.target.value, searchFinished: false
        }, () => {
            if (this.state.characterName.length > 2) {

                const url = `https://gateway.marvel.com:443/v1/public/characters?nameStartsWith=${this.state.characterName}&limit=100&ts=1&apikey=031ff7298eee579ead8f58d2288e0fc3&hash=ea44baf09b4da5db0380665be26ba9d3`

                axios.get(url).then(response => {
                    const results = response.data.data.results
                    console.log(results.length)
                    this.setState({ results: results, currrentPage: 1, searchFinished: true })
                })
            }
        })

    }

    changePage = (event) => {
        this.setState({ currrentPage: event.target.id })
    }

    render() {
        if (!localStorage.getItem('loggedUser')) {
            this.props.history.push('/login')
        }
        const { characterName, results, currrentPage, charactersPerPage, searchFinished } = this.state
        const indexOfLastCharacter = currrentPage * charactersPerPage
        const indexOfFirstCharacter = indexOfLastCharacter - charactersPerPage
        const displayedChars = results.slice(indexOfFirstCharacter, indexOfLastCharacter)

        const renderChars = displayedChars.map(character => {
            return (
                <CharacterCard key={character.id} character={character} />
            )
        })

        const pageNumbers = []
        for (let i = 1; i <= Math.ceil(results.length / charactersPerPage); i++) {
            pageNumbers.push(i)
        }
        const renderPageNums = pageNumbers.map(number => {
            return (
                <React.Fragment key={`${number}-${currrentPage}`}>
                    <input type="radio" name="home pagination radio" className='home-pagination-radio'

                        id={`home-radio-${number}`}
                        defaultChecked={`${number}` === `${currrentPage}` ? true : false} />
                    <label onClick={this.changePage} htmlFor={`home-radio-${number}`} id={number} className='home-pagination-number'> {number} </label>
                </React.Fragment>
            )
        })
        return (
            <div className='home-container'>
                <div className="home-search-container">
                    <input type="text" value={characterName} onChange={this.getCharacters} className='home-search' />
                </div>
                <div className="home-cards-container">{searchFinished === true ?
                    <React.Fragment>
                        {results.length !== 0 && characterName.length !== 0 ? renderChars :
                            <div className='home-search-status-container'>
                                {characterName.length > 2 ?
                                    <h1 className='home-search-status'>No items that match search parameters</h1> :
                                    <h1 className='home-search-status'>Type in at least 3 letters to start the search!!!</h1>}
                            </div>}
                    </React.Fragment> :
                    <div className='home-search-status-container'>{characterName.length > 2 ?
                        <h1 className='home-search-status'>Searching...</h1> :
                        <h1 className='home-search-status'>Type in at least 3 letters to start the search!!!</h1>}
                    </div>}
                </div>
                <div className='home-pagination'> {searchFinished === true && characterName.length > 2 && results.length !== 0 ? renderPageNums : null} </div>
            </div>
        )
    }
}

export default Home