import React from 'react'
import { NavLink } from 'react-router-dom'

const LoggedInLinks = (props) => {
    const { closeNavbar } = props
    const handleClick = () => {
        localStorage.removeItem('loggedUser')
        window.location.reload()
    }

    const loggedUserId = Number(localStorage.getItem('loggedUser'))
    const allUsers = JSON.parse(localStorage.getItem('users'))
    const loggedUsersIndex = allUsers.map(user => {return user.id}).indexOf(loggedUserId)
    const currentUser = allUsers[loggedUsersIndex]
    

    return (
        <div className='logged-in-links-container'>
            <NavLink to='/bookmarks' activeClassName='active-link' onClick={closeNavbar} className='logged-in-link'>Bookmarks</NavLink>
            <NavLink exact to='/' activeClassName='active-link' onClick={closeNavbar} className='logged-in-link'>Home</NavLink>
            <div onClick={handleClick} className='log-out'>Log Out 
                <img src={require('../../../images/Navbar/iron-heart-off.png')} alt="iron heart off" className="log-out-ico-1"/>
                <img src={require('../../../images/Navbar/iron-heart-on.png')} alt="iron heart on" className="log-out-ico-2"/>
            </div>
            <NavLink to='/user_profile' className='profile-initials' onClick={closeNavbar}>  
                {`${currentUser.firstName[0].toUpperCase()}${currentUser.lastName[0].toUpperCase()}`} 
            </NavLink>
        </div>
    )
}

export default LoggedInLinks