import React from 'react'
import { NavLink } from 'react-router-dom'

const LoggedOutLinks = (props) => {
    const {closeNavbar} = props
    return (
        <div className='logged-out-links-container'>
            <NavLink to='/login' activeClassName='active-link' className='logged-out-link' onClick={closeNavbar}>Login</NavLink>
            <NavLink to='/signup' activeClassName='active-link' className='logged-out-link' onClick={closeNavbar}>Sign Up</NavLink>
        </div>
    )
}

export default LoggedOutLinks
