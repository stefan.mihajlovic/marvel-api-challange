import React from 'react'
import { Link } from 'react-router-dom'
import LoggedInLinks from './LoggedInLinks/LoggedInLinks'
import LoggedOutLinks from './LoggedOutLinks/LoggedOutLinks'
import './navbar.scss'

const Navbar = () => {
    const closeNavbar = () => {
        document.getElementById('navCheck').checked = false;
    }
    return (
        <React.Fragment>
            <input type="checkbox" className='navbar-checkbox' id="navCheck"/>
            <label htmlFor="navCheck" className="navbar-label">
                <img src={require('../../images/Navbar/dropdown-icon.png')} alt="dropdown-collapse" className='navbar-label-icon'/>
            </label>
        <div className='navbar-container'>
            <Link to='/' className="nav-logo" onClick={closeNavbar}>MARVEL API CHALLANGE</Link>
            {localStorage.getItem('loggedUser') ? <LoggedInLinks closeNavbar={closeNavbar} /> : <LoggedOutLinks closeNavbar={closeNavbar} />}
        </div>
        </React.Fragment>
    )
}

export default Navbar