import React from 'react'
import './redirect.scss'

const Redirect = (props) => {
    const link = props.location.pathname.replace('/redirect/', '')
    window.location.assign(link)

    return (
        <div className='redirect-container'>
            <h1 className="redirect-title">Redirecting...</h1>
        </div>
    )
}

export default Redirect
