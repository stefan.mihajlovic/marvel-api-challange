import React, { Component } from 'react'
import '../auth.scss'

class Login extends Component {
    state ={
        email: '',
        password:'',
        error: ''
    }

    handleChange = (event) => {
        this.setState({
            [event.target.id]:event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        if (this.state.email !== '' && this.state.password !== '') {
            if (localStorage.getItem('users')) {
                const users = JSON.parse(localStorage.getItem('users'))
                let competeLoop = 0
                for (let i=0; i<users.length; i++) {
                    competeLoop += 1
                    if (users[i].email === this.state.email && users[i].password === this.state.password) {
                        const id = users[i].id
                        localStorage.setItem('loggedUser', id)
                        window.location.reload()
                        break
                    } else if (competeLoop === users.length) {
                        this.setState({error: 'Incorrect email or password'})
                    }
                }
            } else {
                this.setState({error: 'Incorrect email or password'})
            }
        } else {
            this.setState({error: 'Fill out both fields'})
        }
    }

    render() {    
        const {email, password, error} = this.state
        if(localStorage.getItem('loggedUser')) {
            this.props.history.push('/')
        } 
        return (
            <div className='auth-container'>
                <form onSubmit={this.handleSubmit} className="auth-form">
                    <div className="auth-input-container">
                        <input type="text" value={email} onChange={this.handleChange} id='email' className="auth-input"/>
                        <label htmlFor="email" className="auth-label">Email</label>
                    </div>
                    <div className="auth-input-container">
                        <input type="password" value={password} onChange={this.handleChange} id="password" className="auth-input"/>
                        <label htmlFor="password" className="auth-label">Password</label>
                    </div>
                    <div className="auth-error">{error !== '' ? <p className='error-animation'>{error}</p> : null} </div>
                    <input type="submit" value="Login" className="auth-submit"/>
                </form>
            </div>
        )
    }
}

export default Login