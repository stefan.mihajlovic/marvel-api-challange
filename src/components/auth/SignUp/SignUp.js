import React, { Component } from 'react'
import '../auth.scss'

 class SignUp extends Component {
    
    state = {
        id: 0,
        firstName:'',
        lastName:'',
        email:'',
        password:'',
        bookmarks: [],
        errors : {
            fName: 'ok',
            lName: 'ok',
            mail: 'ok',
            password: 'ok'
        }
    }

    handleChange = (event) => {
        this.setState({
            [event.target.id]:event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const errors = {}
        const validMail =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

                if (!localStorage.getItem('users')) {
                    const users = []
                    if (this.state.email === '') {
                        errors['mail'] = 'Mail is requiered'
                    } else if (!validMail.test(this.state.email.toLocaleLowerCase())) {
                        errors['mail'] = 'Enter a valid mail format'
                    }   else {
                        errors['mail'] = 'ok'
                    } 
                    if (this.state.firstName === '') {
                        errors['fName'] = 'First name is requiered'                    
                    } else {
                        errors['fName'] = 'ok'
                    }
                    if (this.state.lastName === '') {
                        errors['lName'] = 'Last name is requiered'
                    } else {
                        errors['lName'] = 'ok'
                    }
                   
                    if (this.state.password === '') {
                        errors['password'] = 'Password is requiered'
                    }else if (this.state.password.length <= 4) {
                        errors['password'] = 'Password must be at least 5 characters long'
                    } else {
                        errors['password'] = 'ok'
                    }
                    if (errors['fName'] === 'ok' && errors['lName'] === 'ok' && errors['mail'] === 'ok' && errors['password'] === 'ok') {
                        users.push(this.state)
                        localStorage.setItem('users', JSON.stringify(users))
                        this.props.history.push('/login')
                    } else {
                        this.setState({errors: errors})
                    }
                } else {    
                    const users = JSON.parse(localStorage.getItem('users'))
                    for (let i = 0; i< users.length; i++) {
                        if (users[i].email === this.state.email) {
                            errors['mail'] = 'Mail already registered'
                            break
                        } else  if (this.state.email === '') {
                            errors['mail'] = 'Mail is requiered'
                            break
                        } else if (!validMail.test(this.state.email.toLocaleLowerCase())) {
                            errors['mail'] = 'Enter a valid mail format'
                            break
                        }   else {
                            errors['mail'] = 'ok'
                            break
                        }  
                    }
                    
                    if (this.state.firstName === '') {
                        errors['fName'] = 'First name is requiered'                    
                    } else {
                        errors['fName'] = 'ok'
                    }
                    if (this.state.lastName === '') {
                        errors['lName'] = 'Last name is requiered'
                    } else {
                        errors['lName'] = 'ok'
                    }
                   
                    if (this.state.password === '') {
                        errors['password'] = 'Password is requiered'
                    }else if (this.state.password.length <= 4) {
                        errors['password'] = 'Password must be at least 5 characters long'
                    } else {
                        errors['password'] = 'ok'
                    }

                    if (errors['fName'] === 'ok' && errors['lName'] === 'ok' && errors['mail'] === 'ok' && errors['password'] === 'ok') {
                        this.setState({id: users[users.length - 1].id + 1}, () => {
                            users.push(this.state)
                            localStorage.setItem('users', JSON.stringify(users))
                            this.props.history.push('/login')
                        })
                    } else {
                        this.setState({errors: errors})
                    }
                }        
    }
    render() {
        if(localStorage.getItem('loggedUser')) {
            this.props.history.push('/')
        } 

        const {firstName,lastName,email,password,errors} = this.state
        return (
            <div className='auth-container'>
                <form onSubmit={this.handleSubmit} className="auth-form">
                    <div className="auth-input-container">
                        <div className="auth-error">{errors.fName !== 'ok' ? <p className='error-animation'>{errors.fName}</p> : null} </div>
                        <input type="text" id="firstName" value={firstName} onChange={this.handleChange} className="auth-input"/>
                        <label htmlFor="firstName" className="auth-label">First Name</label>
                    </div>
                    <div className="auth-input-container">
                        <div className="auth-error">{errors.lName !== 'ok' ? <p className='error-animation'>{errors.lName}</p> : null} </div>
                        <input type="text" id="lastName" value={lastName} onChange={this.handleChange} className="auth-input"/>
                        <label htmlFor="lastName" className="auth-label">Last Name</label>
                    </div>
                    <div className="auth-input-container">
                        <div className="auth-error">{errors.mail !== 'ok' ? <p className='error-animation'>{errors.mail}</p> : null} </div>
                        <input type="text" id="email" value={email} onChange={this.handleChange} className="auth-input"/>
                        <label htmlFor="email" className="auth-label">Email</label>
                    </div>
                    <div className="auth-input-container">
                        <div className="auth-error">{errors.password !== 'ok' ? <p className='error-animation'>{errors.password}</p> : null} </div>
                        <input type="password" id="password" value={password} onChange={this.handleChange} className="auth-input"/>
                        <label htmlFor="password" className="auth-label">Passowrd</label>
                    </div>
                    <input type="submit" value="Sign Up" className="auth-submit"/>  
                </form>
            </div>
        )
    }
}

export default SignUp