import React from 'react'

const EditUser = (props) => {
    const { editUser, newInfo, handleChange, closeEditing, errors } = props
    const { firstName, lastName, email, password } = newInfo
    return (
        <form onSubmit={editUser} className='auth-form'>
            <div onClick={closeEditing} className='close-editing'>X</div>
            <div className="auth-input-container">
                <div className="auth-error">{errors.fName !== 'ok' ? <p className='error-animation'>{errors.fName}</p> : null} </div>
                <input type="text" id="firstName" onChange={handleChange} value={firstName} className="auth-input" />
                <label htmlFor="firstName" className="auth-label">New First Name</label>
            </div>
            <div className="auth-input-container">
                <div className="auth-error">{errors.lName !== 'ok' ? <p className='error-animation'>{errors.lName}</p> : null} </div>
                <input type="text" id="lastName" onChange={handleChange} value={lastName} className="auth-input" />
                <label htmlFor="lastName" className="auth-label">New Last Name</label>
            </div>
            <div className="auth-input-container">
                <div className="auth-error">{errors.mail !== 'ok' ? <p className='error-animation'>{errors.mail}</p> : null} </div>
                <input type="text" id="email" onChange={handleChange} value={email} className="auth-input" />
                <label htmlFor="email" className="auth-label">New Email</label>
            </div>
            <div className="auth-input-container">
                <div className="auth-error">{errors.password !== 'ok' ? <p className='error-animation'>{errors.password}</p> : null} </div>
                <input type="password" id="password" onChange={handleChange} value={password} className="auth-input" />
                <label htmlFor="password" className="auth-label">New Passowrd</label>
            </div>
            <input type="submit" value="Change" className="auth-submit" />
        </form>
    )
}

export default EditUser
