import React, { Component } from 'react'
import EditUser from './EditUser/EditUser'
import './user-profile.scss'

class UserProfile extends Component {
    state = {
        user: '',
        loaded: false,
        editing: false,
        newInfo: {
            firstName: '',
            lastName: '',
            email: '',
            password: ''
        },
        errors : {
            fName: 'ok',
            lName: 'ok',
            mail: 'ok',
            password: 'ok'
        }
    }

    componentDidMount() {
        if (localStorage.getItem('loggedUser')) {
            const userId = Number(localStorage.getItem('loggedUser'))
            const allUsers = JSON.parse(localStorage.getItem('users'))
            const loggedUsersIndex = allUsers.map(user => { return user.id }).indexOf(userId)
            const currentUser = allUsers[loggedUsersIndex]
            this.setState({ user: currentUser, loaded: true })
        }
    }

    openEditing = () => {
        const userId = Number(localStorage.getItem('loggedUser'))
        const allUsers = JSON.parse(localStorage.getItem('users'))
        const loggedUsersIndex = allUsers.map(user => { return user.id }).indexOf(userId)
        const currentUser = allUsers[loggedUsersIndex]
          this.setState({
            newInfo: {
                firstName: currentUser.firstName,
                lastName: currentUser.lastName,
                email: currentUser.email,
                password: currentUser.password
            },errors: {
                fName: 'ok',
                lName: 'ok',
                mail: 'ok',
                password: 'ok'
            } ,editing:true
        })
    }
    closeEditing = () => {
        this.setState({ editing: false })
    }
    handleChange = (event) => {
        event.persist()
        this.setState(prevState => ({
            newInfo: {
                ...prevState.newInfo,
                [event.target.id]: event.target.value
            }
        }))
    }
    editUser = (event) => {
        event.preventDefault()
        const errors = {}
        const userId = this.state.user.id
        const allUsers = JSON.parse(localStorage.getItem('users'))
        const loggedUsersIndex = allUsers.map(user => { return user.id }).indexOf(userId)
        const currentUser = allUsers[loggedUsersIndex]
        const validMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

                for (let i = 0; i < allUsers.length; i++) {
                    if (allUsers[i].email === this.state.newInfo.email && allUsers[i].id !== this.state.user.id) {
                        errors['mail'] = 'Mail already registered'
                        break
                    } else  if (this.state.newInfo.email === '') {
                        errors['mail'] = 'Mail is requiered'
                        break
                    } else if (!validMail.test(this.state.newInfo.email.toLocaleLowerCase())) {
                        errors['mail'] = 'Enter a valid mail format'
                        break
                    }   else {
                        errors['mail'] = 'ok'
                        break
                    }  
                }
                if (this.state.newInfo.firstName === '') {
                    errors['fName'] = 'First name is requiered'                    
                } else {
                    errors['fName'] = 'ok'
                }
                if (this.state.newInfo.lastName === '') {
                    errors['lName'] = 'Last name is requiered'
                } else {
                    errors['lName'] = 'ok'
                }
               
                if (this.state.newInfo.password === '') {
                    errors['password'] = 'Password is requiered'
                }else if (this.state.newInfo.password.length <= 4) {
                    errors['password'] = 'Password must be at least 5 characters long'
                } else {
                    errors['password'] = 'ok'
                }

                if (errors['fName'] === 'ok' && errors['lName'] === 'ok' && errors['mail'] === 'ok' && errors['password'] === 'ok') {
                    currentUser.firstName = this.state.newInfo.firstName
                    currentUser.lastName = this.state.newInfo.lastName
                    currentUser.email = this.state.newInfo.email
                    currentUser.password = this.state.newInfo.password
                    allUsers[loggedUsersIndex] = currentUser
                    localStorage.setItem('users', JSON.stringify(allUsers))
                    window.location.reload()
                } else {
                    this.setState({errors: errors})
                }
           
    }
    render() {
        if (!localStorage.getItem('loggedUser')) {
            this.props.history.push('/login')
        }

        const { user, loaded, editing, newInfo, errors } = this.state
        return (
            <div className='user-profile-container'>
                {!loaded ? <div>Loading....</div>
                    :
                    <React.Fragment>
                        {!editing ?
                            <div className='user-info-container'>
                                <div className="user-info"> {`First Name:${user.firstName}`} </div>
                                <div className="user-info"> {`Last Name: ${user.lastName}`} </div>
                                <div className="user-info"> {`Mail: ${user.email}`} </div>
                                <div className="auth-submit" onClick={this.openEditing}>Edit Info</div>
                            </div>
                            :
                            <EditUser editUser={this.editUser} newInfo={newInfo} closeEditing={this.closeEditing} handleChange={this.handleChange} errors = {errors} />
                        }
                    </React.Fragment>
                }
            </div>
        )
    }
}
export default UserProfile